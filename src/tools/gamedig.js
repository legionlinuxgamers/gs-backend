import Gamedig from 'gamedig';

export class GameTool{
  gdig = new Gamedig();

  async getStatusServer(game, ip){
    var resquey = this.gdig.query({
      type: game,
      host: ip
    }).then((state) => {
      console.log(state);
      return({status: "online",data: state});
    }).catch((error) => {
      console.log(error);
      return({status: "offline",data: {}});
    });
    return resquey
  }

  async getStatusServerOnPort(game, ip, port){
     var resquey = await this.gdig.query({
      type: game,
      host: ip,
      port: port
    }).then((state) => {
      console.log(state);
      return({status: "online",data: state});
    }).catch((error) => {
      console.log(error);
      return({status: "offline",data: {}});
    });

    return resquey
  }

}
