import { GameTool } from './tools/gamedig.js';
import express from 'express';
import fs from 'fs';
import https from 'https';
import http from 'http';

const app = express();
const port = 80;
const portSecure = 3030;
const gTool = new GameTool;

const credentials = {
  key: fs.readFileSync('/etc/letsencrypt/live/patojad.mooo.com/privkey.pem', 'utf8'),
  cert: fs.readFileSync('/etc/letsencrypt/live/patojad.mooo.com/cert.pem', 'utf8'),
  ca: fs.readFileSync('/etc/letsencrypt/live/patojad.mooo.com/chain.pem', 'utf8')
};

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

app.get('/', (req, res) => {
  res.send('El servicio se encuentra funcionando correctamente')
});

// Use for certbot
app.use(express.static('./src', { dotfiles: 'allow' } ));

app.get('/status/:game/:ip', (req, res) => {
  gTool.getStatusServer(req.params.game, req.params.ip)
    .then(response => {
      res.send(response);
    })
    .catch(err => {
      res.send(err);
    });
});

app.get('/status/:game/:ip/:puerto', (req, res) => {
  gTool.getStatusServerOnPort(req.params.game, req.params.ip, req.params.puerto)
    .then(response => {
      res.send(response);
    })
    .catch(err => {
      res.send(err);
    });
});

const httpsServer = https.createServer(credentials, app);
const httpServer = http.createServer(app);

httpServer.listen(port, () => {
  console.log(`HTTPS Server running on port ${port}`);
});

httpsServer.listen(portSecure, () => {
  console.log(`HTTPS Server running on port ${portSecure}`);
});
